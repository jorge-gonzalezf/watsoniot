FROM openjdk:latest
ADD target/rpa-0.0.1-SNAPSHOT.jar rpa-0.0.1-SNAPSHOT.jar
EXPOSE 8085
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "rpa-0.0.1-SNAPSHOT.jar" ]