package com.jorge.gonzalezf.WatsonIoT;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class LogService {

    private String message;
    private String status;
    private String service;
    private final String URL = "https://coe-logging.herokuapp.com/logTransaction";

    public LogService(String message, String status, String service) {
        this.message = message;
        this.status = status;
        this.service = service;
    }

    public String executeAPI() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("message", message);
        map.add("status", status);
        map.add("service", service);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        return restTemplate.postForObject(URL, request, String.class);
    }
}
