package com.jorge.gonzalezf.WatsonIoT;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
public class WatsonController {

    private final String USERNAME = "a-v0harf-s6nhoh8gio";
    private final String PASSWORD = "f5qG*Rqw6dCsMrI8&M";
    private final String URL = "https://v0harf.internetofthings.ibmcloud.com/api/v0002";

    @Autowired
    RestTemplate restTemplate;

    @CrossOrigin
    @RequestMapping("/iot/listDevices")
    public String listDevices() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth(USERNAME, PASSWORD);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange(
                URL + "/device/types/tv/devices",
                HttpMethod.GET, entity, String.class).getBody();
    }

    @CrossOrigin
    @RequestMapping("/iot/getStateDevice")
    public org.json.simple.JSONObject getStateDevice() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(USERNAME, PASSWORD);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        String result = restTemplate.exchange(
                URL + "/device/types/tv/devices/samsung/state/5ddde4a8a920c3001d00121a",
                HttpMethod.GET, entity, String.class).getBody();

        JSONObject jsonObject;
        org.json.simple.JSONObject jsonSimple = new org.json.simple.JSONObject();
        try {
            jsonObject = new JSONObject(result);
            int cont = (int) jsonObject.getJSONObject("state").get("cont");
            jsonSimple.put("cont", cont);

            LogService logService = new LogService(jsonSimple.toJSONString(), "200", "Watson IoT Platform");
            System.out.println(logService.executeAPI());
        } catch (JSONException e) {
            jsonSimple.put("cont", e.getMessage());

            LogService logService = new LogService(e.getMessage(), "500", "Watson IoT Platform");
            System.out.println(logService.executeAPI());
        } finally {
            return jsonSimple;
        }
    }

    @CrossOrigin
    @RequestMapping("/iot/resetStateDevice")
    public org.json.simple.JSONObject resetStateDevice() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(USERNAME, PASSWORD);

        org.json.simple.JSONObject jsonSimple = new org.json.simple.JSONObject();
        jsonSimple.put("operation", "reset-state");
        HttpEntity<String> entity = new HttpEntity<>(jsonSimple.toJSONString(), headers);

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(15000);
        requestFactory.setReadTimeout(15000);
        restTemplate.setRequestFactory(requestFactory);

        String result = restTemplate.exchange(
                URL + "/device/types/tv/devices/samsung/state/5ddde4a8a920c3001d00121a",
                HttpMethod.PATCH, entity, String.class).getBody();

        JSONObject jsonObject;
        jsonSimple = new org.json.simple.JSONObject();
        try {
            jsonObject = new JSONObject(result);
            String message = String.valueOf(jsonObject.get("message"));
            jsonSimple.put("message", message);
        } catch (JSONException e) {
            jsonSimple.put("message", e.getMessage());
        } finally {
            return jsonSimple;
        }
    }
}
